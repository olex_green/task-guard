# Full Stack Engineer Challenge - Alex Tkachuk

## Run

- `docker-compose up -d`

## Usage

- `Open http://localhost:3003` in a Browser

## Further improvements

- Add more Unit Tests
- Implement validation on both - Front-End and Back-End sides
- It is better to use a Front Proxy in production, such as NGINX, Envoy Proxy, Traefik, etc.
- Add React SSR for better initial loading
- Use some logger system instead of console
- Use microservice orchestrator - Docker Swarm or Kubernetes