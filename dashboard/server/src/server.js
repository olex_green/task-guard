const compression = require('compression');
const express = require('express');
const path = require('path');

const PORT = process.env.PORT || 3004;

const app = express();
app.use(compression());
app.get('/test', (req, res) => res.send('ok'));
app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + '/dist/index.html'));
});
app.get('/*', express.static(path.join(__dirname, '/dist')));


app.listen(PORT, '0.0.0.0');

console.log('Running on http://0.0.0.0:3004/');
