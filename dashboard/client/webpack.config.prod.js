const webpackMerge = require('webpack-merge');
const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const nodeExternals = require('webpack-node-externals');

const commonConfig = require('./webpack.config.base.js');


function getProdConfig() {
  return webpackMerge(commonConfig(false), {
    target: 'web',
    entry: path.resolve(__dirname, './src/index.jsx'),
    output: {
      path: `${__dirname}/dist`,
      filename: '[id]-[chunkhash:8].js',
      publicPath: '/',
    },
    optimization: {
      minimize: true,
      splitChunks: {
        chunks: 'all',
        minSize: 250000,
        maxSize: 1500000,
      },
      minimizer: [new TerserPlugin({
        cache: true,
        parallel: true,
      }), new OptimizeCSSAssetsPlugin({})],
    },
  });
}

module.exports = getProdConfig();
