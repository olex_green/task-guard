import axios from 'axios';

const BACK_END_INVALID_MESSAGE = 'Sorry! Something went wrong on our end. Please try again.';

export default async function addResult(result) {
  try {
    const response = await axios.post('http://localhost:3003/results', result);

    if (response && response.data) {
      return response.data.payload;
    }
    throw Error(BACK_END_INVALID_MESSAGE);
  } catch (e) {
    throw Error(BACK_END_INVALID_MESSAGE);
  }
}
