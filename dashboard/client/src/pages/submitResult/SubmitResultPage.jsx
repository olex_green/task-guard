/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { useState } from 'react';

import addResult from './api';

const INITIAL_STATE = {
  findings: '',
  repositoryName: '',
  status: 'Queued',
};

function SubmitResultPage() {
  const [result, setResult] = useState(INITIAL_STATE);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');

  const onChangeHandler = (e) => {
    const { name, value } = e.currentTarget;
    setResult({
      ...result,
      [name]: value,
    });
  };

  const onSubmitHandler = async () => {
    setLoading(true);
    setError('');
    try {
      await addResult(result);
      setResult(INITIAL_STATE);
    } catch (e) {
      setError('Something went wrong, please try again.');
    }
    setLoading(false);
  };

  const onResetHandler = () => {
    setResult(INITIAL_STATE);
  };

  return (
    <div className="container">
      <div className="notification">
        <div className="field">
          <label className="label">Repository Name</label>
          <div className="control">
            <input
              className="input"
              name="repositoryName"
              onChange={onChangeHandler}
              value={result.repositoryName}
              type="text"
            />
          </div>
        </div>

        <div className="field">
          <label className="label">Status</label>
          <div className="control">
            <div className="select">
              <select name="status" onChange={onChangeHandler} value={result.status}>
                <option>Queued</option>
                <option>In Progress</option>
                <option>Success</option>
                <option>Failure</option>
              </select>
            </div>
          </div>
        </div>

        <div className="field">
          <label className="label">Findings</label>
          <div className="control">
            <textarea
              className="textarea"
              name="findings"
              onChange={onChangeHandler}
              placeholder="JSON format as string"
              value={result.findings}
            />
          </div>
        </div>
        <div className="field is-danger">
          {error}
        </div>
        <div className="field is-grouped">
          <div className="control">
            <button
              className={`button is-link${loading ? ' is-loading' : ''}`}
              disabled={loading}
              onClick={onSubmitHandler}
              type="button"
            >
              Submit
            </button>
          </div>
          <div className="control">
            <button
              className="button is-link is-light"
              onClick={onResetHandler}
              type="button"
            >
              Cancel
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SubmitResultPage;
