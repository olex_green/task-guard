import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

import * as logs from '../../common/logs';
import getDetails from './api';

function FindingDetailsPage() {
  const { id } = useParams();
  const [results, setResults] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    async function asyncCall() {
      try {
        setLoading(true);
        const res = await getDetails(id);

        setResults(res);
      } catch (e) {
        logs.error(e);
      }
      setLoading(false);
    }
    asyncCall();
  }, []);

  return (
    <div className="container">
      <div className="notification">
        <table className="table">
          <thead>
            <tr>
              <th>RuleId</th>
              <th>Description</th>
              <th>Severity</th>
              <th>Path</th>
            </tr>
          </thead>
          <tbody>
            {
              loading ? 'loading...'
                : results.map((item) => (
                  <tr>
                    <th>{item.rule_id}</th>
                    <td>{item.description}</td>
                    <td>{item.severity}</td>
                    <td>
                      {item.path}
                      :
                      {item.line}
                    </td>
                  </tr>
                ))
            }
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default FindingDetailsPage;
