import axios from 'axios';

const BACK_END_INVALID_MESSAGE = 'Sorry! Something went wrong on our end. Please try again.';

export default async function get(id) {
  try {
    const response = await axios.get(`http://localhost:3003/results/${id}`);
    if (response && response.data) {
      return response.data.payload;
    }
    throw Error(BACK_END_INVALID_MESSAGE);
  } catch (e) {
    throw Error(BACK_END_INVALID_MESSAGE);
  }
}
