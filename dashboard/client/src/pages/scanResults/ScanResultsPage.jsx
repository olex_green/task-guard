import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

import * as logs from '../../common/logs';
import getResultsList from './api';

function ScanResultsPage() {
  const [results, setResults] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    async function asyncCall() {
      try {
        setLoading(true);
        const res = await getResultsList();

        setResults(res);
      } catch (e) {
        logs.error(e);
      }
      setLoading(false);
    }
    asyncCall();
  }, []);

  return (
    <div className="container">
      <div className="notification">
        <table className="table">
          <thead>
            <tr>
              <th><abbr title="Position">Pos</abbr></th>
              <th>Repository Name</th>
              <th>Status</th>
              <th>Findings</th>
              <th>Timestamp</th>
              <th>Details</th>
            </tr>
          </thead>
          <tbody>
            {
              loading ? 'loading...'
                : results.map((item, index) => (
                  <tr>
                    <th>{index + 1}</th>
                    <td>{item.repositoryName}</td>
                    <td>{item.status}</td>
                    <td>
                      <div className="tags has-addons">
                        <span className="tag is-dark">Count</span>
                        <span className="tag is-danger">
                          {item.findingsCount}
                        </span>
                      </div>
                    </td>
                    <td>{item.updatedAt}</td>
                    <td>
                      <Link className="navbar-item" to={`/results/${item.id}`}>
                        Details
                      </Link>
                    </td>
                  </tr>
                ))
            }
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default ScanResultsPage;
