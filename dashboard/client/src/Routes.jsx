import React from 'react';
import { Switch, Route } from 'react-router-dom';
import loadable from '@loadable/component';

import Loading from './common/components/Loading';
import Layout from './common/components/Layout/Layout';

const ScanResultsPage = loadable(() => import('./pages/scanResults/ScanResultsPage'), {
  fallback: <Loading />,
});
const SubmitResultPage = loadable(() => import('./pages/submitResult/SubmitResultPage'), {
  fallback: <Loading />,
});
const FindingDetailsPage = loadable(() => import('./pages/findingDetails/FindingDetailsPage'), {
  fallback: <Loading />,
});

function Routes() {
  return (
    <Layout>
      <Switch>
        <Route exact path="/" component={SubmitResultPage} />
        <Route exact path="/results" component={ScanResultsPage} />
        <Route exact path="/results/:id" component={FindingDetailsPage} />
        <Route
          component={({ location }) => (
            <div
              style={{
                padding: '50px',
                width: '100%',
                textAlign: 'center',
              }}
            >
              The page
              {' '}
              <code>{location.pathname}</code>
              {' '}
              could not be found.
            </div>
          )}
        />
      </Switch>
    </Layout>
  );
}

export default Routes;
