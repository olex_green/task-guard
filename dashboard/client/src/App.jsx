import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { hot } from 'react-hot-loader';

import './polyfills';
import Routes from './Routes';


function App() {
  return (
    <BrowserRouter>
      <Routes />
    </BrowserRouter>
);
}

export default hot(module)(App);
