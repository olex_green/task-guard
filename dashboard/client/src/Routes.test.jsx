import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';

import { Switch, Route } from 'react-router-dom';

import Routes from './Routes';
import Layout from './common/components/Layout/Layout';
import ErrorBoundary from './ErrorBoundary';
import PrivateRoute from './common/components/PrivateRoute/PrivateRoute';

function setup(specProps) {
  const defaultProps = {
    onChangeUrl: () => {},
    onTestClick: () => {},
  };

  const props = {
    ...defaultProps,
    ...specProps,
  };
  const enzymeWrapper = shallow(<Routes {...props} />);

  return {
    props,
    enzymeWrapper,
  };
}

describe('Routes', () => {
  it('should render self and has first ErrorBoundary', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.first().is(ErrorBoundary)).to.equal(true);
  });

  it('should have 1 PrivateRoute', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.find(PrivateRoute)).to.have.length(1);
  });

  it('should have 9 Route-s', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.find(Route)).to.have.length(9);
  });

  it('should have single Layout', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.find(Layout)).to.have.length(1);
  });

  it('should have single Switch', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.find(Switch)).to.have.length(1);
  });

  describe('First PrivateRoute', () => {
    const { enzymeWrapper } = setup();
    const route = enzymeWrapper.find(PrivateRoute).first();

    it('should have prop exact=true', () => {
      expect(route.prop('exact')).to.equal(true);
    });

    it('should have prop path="/dashboard"', () => {
      expect(route.prop('path')).to.equal('/dashboard');
    });
  });
});
