import React from 'react';
import { Link } from 'react-router-dom';

import './styles.scss';

function Navbar() {
  return (
    <nav className="navbar">
      <div className="container">
        <div className="navbar-brand">
          <div className="navbar-item">
            <Link className="navbar-item" to="/">
              Submit Scan Results
            </Link>
            <Link className="navbar-item" to="/results">
              List of Scan Results
            </Link>
          </div>
        </div>
      </div>
    </nav>
  );
}

export default Navbar;
