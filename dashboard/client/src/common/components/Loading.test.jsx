/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';

import Loading from './Loading';

function setup(props) {
  const enzymeWrapper = shallow(<Loading {...props} />);

  return {
    props,
    enzymeWrapper,
  };
}

describe('Loading', () => {
  it('should render self and has first div', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.first().is('div')).to.equal(true);
  });
});
