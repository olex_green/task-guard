import React from 'react';
import PropTypes from 'prop-types';

import Navbar from '../Navbar/Navbar';
import './layout.scss';

function Layout({
  children,
}) {
  return (
    <div className="layout-root">
      <div className="layout-header">
        <Navbar
          color="primary"
          spaced
        />
      </div>
      <div className="layout-main">
        {children}
      </div>
    </div>
  );
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
