import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';

import Layout from './Layout';
import Navbar from '../Navbar/Navbar';
import Footer from '../Footer/Footer';

function setup(specProps) {
  const defaultProps = {
    children: <div>test</div>,
  };

  const props = {
    ...defaultProps,
    ...specProps,
  };
  const enzymeWrapper = shallow(<Layout {...props} />);

  return {
    props,
    enzymeWrapper,
  };
}

describe('Layout', () => {
  it('should render self and has first div', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.first().is('div')).to.equal(true);
  });

  it('should have single Navbar', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.find(Navbar)).to.have.length(1);
  });

  it('should have single Footer', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.find(Footer)).to.have.length(1);
  });
});
