const QUEUED = 'Queued';
const IN_PROGRESS = 'In Progress';
const SUCCESS = 'Success';
const FAILURE = 'Failure';


module.exports = {
  QUEUED,
  IN_PROGRESS,
  SUCCESS,
  FAILURE,
};
