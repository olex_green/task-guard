const { expect } = require('chai');
const { stub } = require('sinon');

const logger = require('../utils/logs');
const scanResult = require('../services/scanResult');
const { getResults } = require('./resultsController');

describe('getResults', () => {
  context('given getAll throws error', () => {
    it('Should not call res.send and call next, logger.error', async () => {
      const loggerErrorStub = stub(logger, 'error');
      const resStub = {
        send: stub(),
      };
      const nextStub = stub();
      const getAllStub = stub(scanResult, 'getAll').rejects();

      try {
        await getResults({}, resStub, nextStub);
      } finally {
        expect(resStub.send.called).to.equal(false);
        expect(nextStub.called).to.equal(true);
        expect(loggerErrorStub.called).to.equal(true);
      }

      getAllStub.restore();
      loggerErrorStub.restore();
    });
  });

  context('given getAll resolves value', () => {
    it('Should call res.send and not call: next, logger.error', async () => {
      const loggerErrorStub = stub(logger, 'error');
      const resStub = {
        send: stub(),
      };
      const nextStub = stub();
      const getAllStub = stub(scanResult, 'getAll').resolves();

      try {
        await getResults({}, resStub, nextStub);
      } finally {
        expect(resStub.send.called).to.equal(true);
        expect(nextStub.called).to.equal(false);
        expect(loggerErrorStub.called).to.equal(false);
      }

      getAllStub.restore();
      loggerErrorStub.restore();
    });
  });
});
