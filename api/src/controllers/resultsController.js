const logger = require('../utils/logs');

const scanResult = require('../services/scanResult');

async function getResults(req, res, next) {
  try {
    const results = await scanResult.getAll();

    res.send({
      payload: results,
    });
  } catch (e) {
    logger.error('getResults controller failed', e);
    next(e);
  }
}

async function addResult(req, res, next) {
  try {
    const {
      status,
      repositoryName,
      findings,
    } = req.body;

    const results = await scanResult.add({
      status,
      repositoryName,
      findings,
    });

    res.send({
      payload: results,
    });
  } catch (e) {
    logger.error('getResults controller failed', e);
    next(e);
  }
}

async function getFindingsDetails(req, res, next) {
  try {
    const { id } = req.params;

    const findings = await scanResult.getFindingsById(id);

    res.send({
      payload: findings,
    });
  } catch (e) {
    logger.error('getFindingsDetails controller failed', e);
    next(e);
  }
}

module.exports = {
  addResult,
  getResults,
  getFindingsDetails,
};
