const knex = require('knex');

const { getDbConnectionSettings } = require('../utils/secrets');


function connectToDB() {
  if (!global.db) {
    const settings = getDbConnectionSettings();
    global.db = knex({
      client: 'pg',
      connection: {
        host: settings.host,
        port: settings.port,
        user: settings.user,
        password: settings.password,
        database: settings.database,
      },
    });
  }

  return global.db;
}

module.exports = {
  connectToDB,
};
