/* eslint-disable no-multi-str */
const dbHelper = require('./dbHelper');
const logs = require('../utils/logs');

async function initDb() {
  const db = dbHelper.connectToDB();
  try {
    try {
      await db.raw('create extension "uuid-ossp";');
    } catch (e) {
      throw new Error(e.message);
    }

    await db.raw('CREATE TABLE results\
      (\
          id uuid NOT NULL DEFAULT uuid_generate_v1(),\
          status varchar(20) NOT NULL,\
          repository_name varchar(255) NOT NULL,\
          findings json NOT NULL,\
          queued_at timestamp,\
          scanning_at timestamp,\
          finished_at timestamp,\
          PRIMARY KEY (id)\
      )\
      WITH (\
          OIDS = FALSE\
      );\
    ');
  } catch (e) {
    logs.info('Init DB error: ', e.message);
  }
}

module.exports = {
  initDb,
};
