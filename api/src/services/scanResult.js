const dbHelper = require('../db/dbHelper');
const logger = require('../utils/logs');

const STATUSES = require('../const/statuses');

async function getAll() {
  const db = dbHelper.connectToDB();

  try {
    const results = await db('results')
      .select(
        db.raw('id, status, repository_name, json_array_length(findings -> \'findings\') as findings_count, queued_at, scanning_at, finished_at'),
      );

    return results
      .map((item) => {
        let updatedAt;

        switch (item.status) {
          case STATUSES.QUEUED:
            updatedAt = item.queued_at;
            break;
          case STATUSES.IN_PROGRESS:
            updatedAt = item.scanning_at;
            break;
          default:
            updatedAt = item.finished_at;
            break;
        }

        return {
          id: item.id,
          repositoryName: item.repository_name,
          status: item.status,
          findingsCount: item.findings_count,
          updatedAt,
        };
      });
  } catch (e) {
    logger.error('getAll DB error: ', e);

    throw new Error('getAll DB error');
  }
}

async function add({
  status,
  repositoryName,
  findings,
}) {
  const db = dbHelper.connectToDB();
  try {
    const [id] = await db('results')
      .returning('id')
      .insert({
        status,
        repository_name: repositoryName,
        findings,
        queued_at: status === STATUSES.QUEUED ? new Date().toUTCString() : undefined,
        scanning_at: status === STATUSES.IN_PROGRESS ? new Date().toUTCString() : undefined,
        finished_at: status === STATUSES.FAILURE || status === STATUSES.SUCCESS
          ? new Date().toUTCString() : undefined,
      });

    return id;
  } catch (e) {
    logger.error('add DB error: ', e.message);
    return '';
  }
}

async function getFindingsById(id) {
  const db = dbHelper.connectToDB();

  try {
    const { rows } = await db.raw(`
      Select findings -> 'ruleId' as rule_id,
        findings -> 'metadata' -> 'description' as description,
        findings -> 'metadata' -> 'severity' as severity,
        findings -> 'location' -> 'path' as path,
        findings -> 'location' -> 'positions' -> 'begin' -> 'line' as line
      From (
        Select json_array_elements(findings -> 'findings') as findings
        From public.results
        Where id = ?
      ) as tmp`,
    [id]);

    return rows;
  } catch (e) {
    logger.error('getFindingsById DB error: ', e);

    throw new Error('getFindingsById DB error');
  }
}

module.exports = {
  add,
  getAll,
  getFindingsById,
};
