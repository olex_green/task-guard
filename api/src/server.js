const express = require('express');
const cors = require('cors')
const bodyParser = require('body-parser');

const { initDb } = require('./db/init');
const {
  addResult,
  getResults,
  getFindingsDetails,
} = require('./controllers/resultsController');
const logger = require('./utils/logs');

const PORT = process.env.PORT || 3003;

async function main() {
  try {
    await initDb();
  } catch (e) {
    logger.error('initDb error:', e);
  }
  const app = express();

  app.use(cors({ origin: 'http://localhost:3004' }))
  app.use(bodyParser.json());
  app.get('/results', getResults);
  app.post('/results', addResult);
  app.get('/results/:id', getFindingsDetails);

  app.listen(PORT);

  logger.info(`Running on http://localhost:${PORT}/`);
}

main();
