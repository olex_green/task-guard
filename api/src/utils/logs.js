const signale = require('signale');

signale.config({
  displayTimestamp: true,
  displayDate: true,
});

function error(msg, e) {
  signale.error(msg, e);
}

function info(msg, ...optionalParams) {
  signale.log(msg, ...optionalParams);
}

module.exports = {
  error,
  info,
};
