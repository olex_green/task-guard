function getDbConnectionSettings() {
  return {
    host: process.env.DB_HOST || '0.0.0.0',
    port: process.env.DB_PORT || 5437,
    user: process.env.DB_USER || 'pg',
    password: process.env.DB_PASSWORD || 'pg',
    database: process.env.DB_NAME || 'pg_test',
  };
}

module.exports = {
  getDbConnectionSettings,
};
